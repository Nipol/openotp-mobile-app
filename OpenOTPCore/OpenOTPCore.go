package OpenOTPCore

import (
	"fmt"
	"net/http"
	"encoding/json"
	"hash/crc64"
	"crypto/md5" 
	"time"
	"strconv"
)

type oneMinuteKey struct {
	Prefix	int
	Year	int
	Month	int
	Day		int
	Hour	int
	Minute	int
	Key	string
}

var TodayKeys [1440]oneMinuteKey

func minuteKeyGen(year, month, day, hour, min int) string {
	h := md5.New()
	h.Write([]byte( strconv.FormatUint(crc64.Checksum([]byte(string(year)+string(month)+string(day)+string(hour)+string(min)+"OpenOTP"), crc64.MakeTable(crc64.ISO)), 30) ))
	return fmt.Sprintf("%x", string(h.Sum(nil)))
}

func KeyInitial () {
	count := 0
	daily := time.Now()
	for Hcount := 0; Hcount <= 23; Hcount++ {
		for Mcount := 0; Mcount <= 59; Mcount++ {
			TodayKeys[count].Prefix		= count
			TodayKeys[count].Year		= daily.Year()
			TodayKeys[count].Month		= int(daily.Month())
			TodayKeys[count].Day		= daily.Day()
			TodayKeys[count].Hour		= Hcount
			TodayKeys[count].Minute		= Mcount
			TodayKeys[count].Key		= minuteKeyGen(TodayKeys[count].Year, TodayKeys[count].Month, TodayKeys[count].Day, TodayKeys[count].Hour, TodayKeys[count].Minute)
			count++
		}
	}
}

// index Page Handler
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	//w.Header().Add("Cache-control", "public, max-age=604800")
	indexPage.Execute(w, nil)
}

// openKey Page Handler
// this page's json format
func OpenKeyHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-type", "application/json")
	funcname := r.URL.Query().Get("callback")
	
	KeyInitial();
	//res, _ := json.MarshalIndent(TodayKeys,"","\t")
	res, _ := json.Marshal(TodayKeys)
	openkeyPage.Execute(w, ""+funcname+"("+string(res)+");")
}

// OpenOTPCore pkg initializer
func init() {

}
