package OpenOTPCore

import "text/template"

var indexPage = template.Must(template.New("Index").Parse(`
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes"> 
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	
	<title>OpenOTP</title>
	
	<!-- OpenOTP CSS -->
	<link href="/css/oo.css" rel="stylesheet" type="text/css"/>
	
</head>
<body>
	<!--
		PasswordPage - 앱을 실행할때 패스워드 입력창을 제공한다.
		5번 틀렸을때 모든 데이터 초기화.
	-->
	<div id="password" style="display:block">
		<header>
			<div class="title">OpenOTP</div>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling"><div class="vflex scrolling"><div class="bounce-fix vflex">
			
			<center>
			<div id="passwordinputbox">
				<input type="password" maxlength="4" autocomplete="off" autocapitalize="off" autocorrect="off" autofocus/>
			</div>
			<div id="passwordstatus">
				<span>Status</span>
			</div>
			</center>
			
			</div></div></div>
		</section>
	</div>
		
	<!--
		indexPage - 사용자가 등록한 서비스들의 리스트를 출력한다.
		Left  Button : aboutPage 이동
		Right Button : addPage 이동
		List         : OTPPage 이동
		Left  footer : 없음
		Right footer : 없음
	-->
	<div id="index" style="display:none">
		<header>
			<a href="#about" id="about"><button class="button left noclickdelay">★</button></a>
			<div class="title">OpenOTP</div>
			<a href="#add" id="add"><button class="button right noclickdelay">+</button></a>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling">
				<div class="vflex scrolling">
					<div class="bounce-fix vflex">
						<ul id="servicelist">
						</ul>
					</div>
				</div>
			</div>
		</section>
		<!--
		<footer>
		</footer>
		-->
	</div><!-- <div id="index"> -->
	
	<!--
		otpPage - 사용자가 선택한 서비스의 OTP를 표시한다.
		Left  Button : indexPage 이동
		Right Button : 없음
		List         : 없음
		Left  footer : OTP 출력 페이지
		Right footer : 정보 수정 페이지
	-->
	<div id="otp" style="display:none">
		<header>
			<a href="#index" id="backbutton"><button class="button left noclickdelay">◀</button></a>
			<!-- 헤더가 동적으로 자리 잡을 곳. -->
			<div class="title"></div>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling">
				<div class="vflex scrolling">
					<div class="bounce-fix vflex">
						<div id="otpdisplay">
							<center>
							<span id="otp"></span>
							<br/>
							<br/>
							<div id="progress-bar">
								<div id="status"></div>
							</div>
							</center>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--
		<footer>
		</footer>
		-->
	</div><!-- <div id="OTP"> -->
	
	<!--
		addPage - 사용자가 선택한 서비스의 OTP를 표시한다.
		Left  Button : indexPage 이동
		Right Button : 기입한 정보 저장, indexPage 이동
		List         : 정보 입력란.
		Left  footer : 없음
		Right footer : 없음
	-->
	<div id="add" style="display:none">
		<header>
			<a href="#index" id="backbutton"><button class="button left noclickdelay">◀</button></a>
			<div class="title">Add</div>
			<a href="#index" id="confirm" style="display:none;"><button class="button right noclickdelay">✔</button></a>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling">
				<div class="vflex scrolling">
					<div class="bounce-fix vflex">
						<div style="margin:10px 5px 10px 5px;"><label>Name. </label><input id="addservicename" ss="s" type="text"/></div>
						<ul id="addlist">
						</ul>
						<span><center>
						<a id="addform" style="color:black; font-size:16pt;">+</a>
						</span></center>
					</div>
				</div>
			</div>
		</section>
		<!--
		<footer>
		</footer>
		-->
	</div><!-- <div id="add"> -->
	
	<!--
		modifyPage - 사용자가 추가한 서비스의 정보를 수정한다.
		Left  Button : indexPage 이동
		Right Button : 없음
		List         : 정보 입력란.
		Left  footer : OTP 출력 페이지
		Right footer : 정보 수정 페이지
	-->
	<div id="modify" style="display:none">
		<header>
			<a href="#index" id="backbutton"><button class="button left noclickdelay">◀</button></a>
			<div class="title"></div>
			<a href="#index" id="confirm"><button class="button right noclickdelay">✔</button></a>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling">
				<div class="vflex scrolling">
					<div class="bounce-fix vflex">
						<div style="margin:10px 5px 10px 5px;"><label>Name. </label><input id="modservicename" ss="s" type="text"/></div>
						<ul id="modifylist">
						</ul>
						<span><center>
						<a id="addform" style="color:black; font-size:16pt;">+</a>
						</span></center>
					</div>
				</div>
			</div>
		</section>
		<!--
		<footer>
		</footer>
		-->
	</div><!-- <div id="modify"> -->
	
	<!--
		aboutPage - OpenOTP에 대한 설명을 출력한다.
		Left  Button : indexPage 이동
		Right Button : 없음
		List         : OpenOTP 정보 출력
		Left  footer : 없음
		Right footer : 없음
	-->
	<div id="about" style="display:none">
		<header>
			<a href="#index" id="backbutton"><button class="button left noclickdelay">◀</button></a>
			<div class="title">About</div>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling">
				<div class="vflex scrolling">
					<div class="bounce-fix vflex">
						<br/>
						<span><center><h1>OpenOTP</h1><h4>ver 0.9</h4></center></span>
						<br/>
						<h4>OpenOTP는 무엇인가요?</h4>
						<div>OpenOTP는 누구나 무료로 사용할 수 있는 일회용 비밀번호 생성(OTP, One Time Password) 서비스 입니다.</div><br/>
						<h4>OpenOTP는 어떻게 동작하나요?</h4>
						<div>OpenOTP는 사용자를 분별 할 수 있는 최대 20가지 개인정보를 통해 일회용 비밀번호를 생성하여, 사용자를 서비스에 인증 할 수 있도록 도와줍니다.</div><br/>
						<h4>OpenOTP는 개인정보를 어떻게 관리하나요?</h4>
						<div>개인정보는 복호화가 불가능한 Hash 문자열(sha256)로 변환되어. 절대 서버로 전송되지 않고, 사용자의 컴퓨터 또는 모바일 디바이스에 저장되어 관리됩니다.</div><br/>
						<h4>OpenOTP의 사용방법.</h4>
						<div>서비스에 입력한 개인정보를 순서대로 OpenOTP에 똑같이 입력하여 등록합니다. 그리고 생성된 7자리 비밀번호를 로그인 창에 입력 하기만 하면 됩니다.</div><br/>
					</div>
				</div>
			</div>
		</section>
		<!--
		<footer>
		</footer>
		-->
	</div><!-- <div id="about"> -->
	
	<!--
		Easteregg - OpenOTP에 대한 설명을 출력한다.
		Left  Button : indexPage 이동
		Right Button : 없음
		List         : OpenOTP 정보 출력
		Left  footer : 없음
		Right footer : 없음
	-->
	<div id="Easteregg" style="display:none">
		<header>
			<a href="#index" id="backbutton"><button class="button left noclickdelay">◀</button></a>
			<div class="title">Easter Egg</div>
			<a href="#easteregg"><button class="button right noclickdelay">Egg</button></a>
		</header>
		<section class="scrollwrapper">
			<div class="master vflex scrolling">
				<div class="vflex scrolling">
					<div class="bounce-fix vflex">
					</div>
				</div>
			</div>
		</section>
	</div><!-- <div id="about"> -->
	<!-- Jquery Module -->
	<script src="/js/jquery-1.7.2.min.js" type="text/javascript"></script>
	<!-- CosmOS Module -->
	<script src="/js/CosmOS.js" type="text/javascript"></script>
	<!-- OpenOTP Module -->
	<script src="/js/OpenOTP.js" type="text/javascript"></script>
	<!-- Crypto-JS Module -->
	<script src="/js/sha256.js" type="text/javascript"></script>
</body>
</html>
`))

var openkeyPage = template.Must(template.New("Openkey").Parse(`{{.}}`))