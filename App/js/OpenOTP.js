// 이전에 위치하였던 해쉬값을 기억하는공간.
var prevlocation;
// 폼의 갯수를 저장하는 공간.
var unitform;
// 선택한 서비스의 넘버.
var servicenum;
// OTP Timer의 아이디
var timerid;
// password의 순서.
var passorder;
// password의 저장공간.
var passdata;
//패스워드 확인
var firstpass;

$(function(){
	if(!window.navigator.standalone) {		//Web Mode
		document.location = "#password";
	} else {								//App Mode
		document.location = "#password";
	}

	// 저장된 서비스들의 갯수
	if( localStorage.getItem("#unit") == null )
		localStorage.setItem("#unit", 0);
	
	//주소창에 해쉬를 입력하지 않았을때 인덱스 페이지로 인식하게 한다.
	unitform = 0;
	
	// 패스워드 설정.
	passwordregist();
	
	// 서비스들의 출력
	DisplayServices();
	
	// 화면의 기울기가 변경 되었을때 스크롤을 가장 위로 올린다.
	$(window).live('orientationchange', function() {
		window.scrollTo(0, 0);
	});
	// 패스워드 입력 상단 움직임 금지
	$("#passwordview").live('touchmove', function() {
		return false;
	});
	// 패스워드 입력 하단 움직임 금지
	$("#passwordinput").live('touchmove', function() {
		return false;
	});
	//UI 헤더의 움직임 금지.
	$("header").live('touchmove', function() {
		return false;
	});
	//UI 푸터의 움직임 금지.
	$("footer").live('touchmove', function() {
		return false;
	});
	//UI 버튼 누름에 따른 컬러 변경
	$("button.button").live('touchstart touchend', function() {
		$(this).toggleClass('over');
	});
	// otp와 modify 페이지로 넘어갈 때에 서비스 번호 리턴.
	$("a[href=#otp], a[href=#modify]").live('click', function() {
		servicenum = $(this).attr("value");
	});

	if(location.hash.substr(1) == "") {
		prevlocation = $("div#index");
	} else {
		prevlocation = $("div[id="+location.hash.substr(1)+"]");
	}

	//주소창에 해쉬값을 입력 하였을때 해상 페이지로 redirection하기 위함
	$("a[href="+location.hash+"]").click();
	$(window).trigger('hashchange');
	$(window).bind('hashchange', function(event) {
		switch(location.hash) {
			case "#password":
				prevlocation.hide();
				$("div#password").show();
				break;
			/*
			 * indexPage 의 해쉬처리.
			 * 이전 페이지 숨김.
			 * indexPage 나타남.
			 */
			case "#index":
				// 오늘의 키 업데이트
				TodaykeyUpdate();
				prevlocation.hide();
				$("div#index").show();
				break;
				
			/*
			 * otpPage 의 해쉬처리.
			 * 이전 페이지 숨김.
			 * otpPage 나타남.
			 */	
			case "#otp":
				DisplayOTP(servicenum);
				prevlocation.hide();
				$("div#otp").show();
				break;
			/*
			 * addPage 의 해쉬처리.
			 * 이전 페이지 숨김.
			 * addPage 나타남.
			 */	
			case "#add":
				prevlocation.hide();
				$("div#add").show();
				break;
			/*
			 * ModifyPage 의 해쉬처리.
			 * 이전 페이지 숨김.
			 * ModifyPage 나타남.
			 */	
			case "#modify":
				DisplayModify(servicenum);
				prevlocation.hide();
				$("div#modify").show();
				break;
			/*
			 * aboutPage 의 해쉬처리.
			 * 이전 페이지 숨김.
			 * aboutPage 나타남.
			 */	
			case "#about":
				prevlocation.hide();
				$("div#about").show();
				break;
			/*
			 * eastereggPage 의 해쉬처리.
			 * 이전 페이지 숨김.
			 * eastereggPage 나타남.
			 */	
			case "#easteregg":
				prevlocation.hide();
				$("div#Easteregg").show();
				break;
		}
		// 이동한 페이지가 otp페이지가 아니며,
		// 이전에 위치하고 있던 페이지가 otp페이지 라면 헤더를 삭제한다.
		if( prevlocation.attr("id") === $("div#otp").attr("id") ) {
			$("div#otp header div.title").html("");
			clearTimeout(timerid);
		}
		
		// 이전 위치값을 새로 저장한한다.
		prevlocation = $("div[id="+location.hash.substr(1)+"]");
		
		// 서비스들의 출력
		DisplayServices();
	}); //$(window).bind('hashchange', function(event) {
	
	/*
	 *  otpPage
	 *  back 버튼 클릭.
	 */
	$("div#otp a#backbutton").live('click', function() {
		$("div#otp header div.title").html("");
		$("div#otpdisplay span:first").html("");
		$("#status").animate( { width: "0px" }, 0);
	});
	
	/*
	 *  AddPage
	 *  Back Button누름. 확인
	 */
	$("div#add a#backbutton").live('click', function() {
		if( ($("input#addservicename").val() != "") || unitform > 0 ) {
			if( confirm("입력한 내용이 모두 삭제됩니다.") ) {
				$("input#addservicename").val("");
				unitform = 0;
				//추가된 form들을 모두 지운다.
				$("ul#addlist li").remove();
				//AddPage의 confirm버튼을 안보이게 한다.
				$("div#add a#confirm").hide();
			} else {
				return false;
			}
		}
	});
	
	/*
	 *  AddPage
	 *  Form 추가 버튼 클릭.
	 */
	$("div#add a#addform").live('click touchend', function() {
		if(unitform < 20) {
			$("ul#addlist").append("<li>"+fillzero(unitform,2)+". <input id=\"addval\" val="+unitform+" style=\"width:270px;\" type=\"text\"/></li>");
			unitform++;
		} else {
			alert("20개가 넘는 비밀키는 추가 할 수 없습니다.");
		}
		//AddPage의 confirm버튼을 생기게 한다.
		$("div#add a#confirm").show();
	});
	
	/*
	 *  AddPage
	 *  Confirm 버튼 클릭.
	 */
	$("div#add a#confirm").live('click', function() {
		var name = $("input#addservicename").val();
		var adderror = 0;
		
		for(count=0; count<unitform; count++) {
			if($("input[id=\"addval\"][val="+count+"]").val() == "")
				adderror = 3;
		}
		
		if( localStorage.getItem(name) != null ) {
			adderror = 2;
		}
		
		if (name == "") {
			adderror = 1;
		}
		
		if (adderror === 1) {
			alert("이름이 입력되지 않았습니다.\n이름을 입력하세요.");
			return false;
		} else if (adderror === 2) {
			alert("이미 존재하는 이름 입니다.\n다른 이름을 입력하세요.");
			return false;
		} else if (adderror === 3) {
			alert("입력되지 않은 빈 공간이 있습니다.");
			return false;
		} else if (adderror === 0) {
			if( confirm("입력된 내용이 모두 정확합니까?") ) {
				//서비스 갯수 추가.
				
				//저장소의 마지막공간에 데이터 삽입.
				var num = localStorage.getItem("#unit");
				localStorage.setItem(num, name);
				
				for(count=0; count<unitform; count++) {
					data = CryptoJS.SHA256($("input[id=\"addval\"][val="+count+"]").val()).toString();
					localStorage.setItem(""+name+"#"+count+"", data );
				}
				
				num++;
				localStorage.removeItem("#unit");
				localStorage.setItem("#unit", num);
				
				delete datalist;
				//내용 초기화 작업.
				$("input#addservicename").val("");
				unitform = 0;
				//추가된 form들을 모두 지운다.
				$("ul#addlist li").remove();
				//AddPage의 confirm버튼을 안보이게 한다.
				$("div#add a#confirm").hide();
			} else {
				return false;
			}
		}
	});
	
	/*
	 *  modifyPage
	 *  back 버튼 클릭.
	 */
	$("div#modify a#backbutton").live('click', function() {
		var modcount = 0;
		for(count=0; count<unitform; count++) {
			if($("input[id=\"modval\"][val="+count+"]").val() != "Saved") {
				modcount++;
			}
		}
		
		if(modcount == 0) {
			unitform = 0;
			$("ul#modifylist li").remove();
			$("div#modify div.title").html("");	
		} else {
			if( confirm("수정한 사항을 저장하지 않고,\n첫 화면으로 돌아갑니다.") ) {
				unitform = 0;
				$("ul#modifylist li").remove();
				$("div#modify div.title").html("");	
			} else {
				return false;
			}
		}
	});
	
	/*
	 *  modifyPage
	 *  addForm 버튼 클릭.
	 */
	$("div#modify a#addform").live('click touchend', function() {
		if(unitform < 20) {
			$("ul#modifylist").append("<li>"+fillzero(unitform, 2)+". <input id="+unitform+" style=\"width:270px;\" type=\"text\"/></li>");
			unitform++;
		} else {
			alert("20개가 넘는 비밀키는 추가 할 수 없습니다.");
		}
	});
	
	/*
	 *  modifyPage
	 *  confirmForm 버튼 클릭.
	 */
	$("div#modify a#confirm").live('click', function() {
		if(confirm("수정한 정보를 반영 하시겠습니까?")) {
			var name = $("div#modify div.title").text();
			var modname = $("input#modservicename").val();
			var serId = servicenum;
			var prevdata = localStorage.getItem(name);
			
			if(name != modname) {
				localStorage.removeItem(serId);
				localStorage.setItem(serId, modname);
				for(count=0; count<unitform; count++) {
					value = localStorage.getItem(""+name+"#"+count+"");
					localStorage.setItem(""+modname+"#"+count+"", value);
					localStorage.removeItem(""+name+"#"+count+"");
				}
				name = modname;
			}
			
			for(count=0; count<unitform; count++) {
				if($("input[id=\"modval\"][val="+count+"]").val() != "Saved") {
					value = CryptoJS.SHA256( $("input[id=\"modval\"][val="+count+"]").val() ).toString();
					if( localStorage.getItem(""+modname+"#"+count+"") === value )
						continue;
					else {
						if( localStorage.getItem(""+modname+"#"+count+"") != null ) {
							localStorage.removeItem(""+modname+"#"+count+"");
							localStorage.setItem(""+modname+"#"+count+"", value);
						} else {
							localStorage.setItem(""+modname+"#"+count+"", value);
						}
					}
				}
			}
		}
		unitform = 0;
		$("ul#modifylist li").remove();
		$("div#modify div.title").html("");
	});
});

function DisplayModify (num) {
	var servicename = localStorage.getItem(num);
	$("div#modify div.title").html(""+servicename+"");
	$("input#modservicename").val(""+servicename+"");
	for(count=0; count<20; count++) {
		if( localStorage.getItem(""+servicename+"#"+count+"") != null ) {
			$("ul#modifylist").append("<li>"+fillzero(count,2)+". <input id=\"modval\" val="+count+" style=\"width:270px;\" type=\"text\" value=\"Saved\" /></li>");
			unitform++;
		} else {
			count = 0;
			break;
		}
	}
}

function GenOTP (name) {
	bigstring = new String;
	var hashprev = new String;
	var hash = new Array;
	var now = new Date();
	var hour = now.getHours();
	var minute = now.getMinutes();
	var key = localStorage.getItem("@"+hour+"/"+minute+"");
	for(count=0; count<20; count++) {
		if(localStorage.getItem(""+name+"#"+count+"") != null)
			bigstring += localStorage.getItem(""+name+"#"+count+"");
		else
			break;
	}
	bigstring += key;
	hashprev = CryptoJS.SHA256(bigstring).toString().slice(10,17);
	for(i=0; i<7; i++) {
		if( hashprev.charCodeAt(i) > 96 )
			hash[i] = hashprev.charCodeAt(i)-96;
		else
			hash[i] = Number(hashprev[i]);
	}
	return hash;
}

function DisplayOTP (num) {
	var pass = new Array;
	$("div#otp header div.title").html(""+localStorage.getItem(num)+"");
	pass = GenOTP(localStorage.getItem(num));
	$(function timeclock() {
		timerid = setTimeout(function() {
			var now = new Date();
			var second = now.getSeconds();
			if(second == 0) {
				pass = GenOTP(localStorage.getItem(num));
				$("div#otpdisplay span:first").html("<span>"+pass[0]+" "+pass[1]+" "+pass[2]+" "+pass[3]+" "+pass[4]+" "+pass[5]+" "+pass[6]+"</span>");
				$("#status").animate( { width: "177px" }, 50);
			}
			$("div#otpdisplay span:first").html("<span>"+pass[0]+" "+pass[1]+" "+pass[2]+" "+pass[3]+" "+pass[4]+" "+pass[5]+" "+pass[6]+"</span>");
			$("#status").animate( { width: ""+(59-second)*3+"px" }, 200);
			timeclock();
		}, 1000);
	});
}

function DisplayServices () {
	if(localStorage.getItem("#unit") >= 0) {
		//이미 존재하는 리스트들은 삭제한다.
		$("ul#servicelist li").remove();
		//새롭게 불러와 붙인다.
		for(count=0; count<localStorage.getItem("#unit"); count++)
			$("ul#servicelist").append("<li>"+
										"<div id=\"leftlist\">"+
											"<a href=\"#otp\" value="+count+">"+
												"<div>"+localStorage.getItem(count)+"</div>"+
											"</a>"+
										"</div>"+
										"<div id=\"rightlist\">"+
											"<a href=\"#modify\" id=\"modifyservice\" value="+count+">"+
												"<div style=\"width:44px;\">❖</div>"+
											"</a>"+
										"</div>"+
										"</li>");
	}
}

function TodaykeyUpdate () {
	var now = new Date();
	var day = localStorage.getItem("#day")
	// 지금 날짜와 저장된 날짜가 다른 경우.
	if(now.getDate() != day) {
		$.ajax({
			url:	"http://localhost:8090/openkey/?callback=?",
			dataType:	'jsonp',
			jsonp:'callback',
			success:function(data) {
				deleteKeys(day);
				localStorage.setItem("#month", data[0].Month);
				localStorage.setItem("#day", data[0].Day);
				$.each(data, function(index, value) {
					localStorage.setItem("@"+value.Hour+"/"+value.Minute+"",  value.Key);
				});
				alert(""+localStorage.getItem("#month")+"/"+localStorage.getItem("#day")+" 업데이트를 완료 하였습니다.");
			},
			error:	function(data) {
				alert("error");
			},
		});
	}
}

function deleteKeys (day) {
	localStorage.removeItem("#today");
	for (hcount=0; hcount<=23; hcount++) {
		for (mcount=0; mcount<=59; mcount++) {
			localStorage.removeItem("@"+hcount+"/"+mcount+"");
		}
	}
}

function passwordregist () {
	// 패스워드 순서
	passorder = 0;
	// 패스워드 저장 공간
	passdata = "";
	// 패스워드 확인
	firstpass = "";
	
	if ( localStorage.getItem("#pass") == null ) {
		$("section div#passwordstatus span").html("정보의 보호를 위해 암호를 설정합니다.<br/>패스워드를 입력해 주세요.");
	}
}
