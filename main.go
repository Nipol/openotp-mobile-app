package main

import (
	"fmt"
	"net/http"
	"compress/gzip"
	"strings"
	"bytes"
	"./OpenOTPCore"
)

type gzipResponseWriter struct {
	http.ResponseWriter
	buf bytes.Buffer
}

func (w *gzipResponseWriter) Write(b []byte) (int, error) {
	return w.buf.Write(b)
}

func makeGzipHandler(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			fn(w, r)
			return
		}
		w.Header().Set("Content-Encoding", "gzip")
		gzw := &gzipResponseWriter{ResponseWriter: w}
		fn(gzw, r)
		w.Header().Set("Content-Type", http.DetectContentType(gzw.buf.Bytes()))
		gz := gzip.NewWriter(w)
		defer gz.Close()
		gzw.buf.WriteTo(gz)
	}
}

func main() {
	fileServer := http.StripPrefix("/js/", http.FileServer(http.Dir("App/js")))
	http.Handle("/js/", fileServer)
	fileServer = http.StripPrefix("/css/", http.FileServer(http.Dir("App/css")))
	http.Handle("/css/", fileServer)
	
	http.HandleFunc("/", 			makeGzipHandler(OpenOTPCore.IndexHandler))
	http.HandleFunc("/openkey/",	OpenOTPCore.OpenKeyHandler)
	
	fmt.Println("Open Success! http://localhost:8090")
	http.ListenAndServe(":8090", nil)
}

